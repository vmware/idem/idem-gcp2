from typing import Any
from typing import Dict
from typing import List

import pytest

from tests.utils import generate_unique_name

RESOURCE_TYPE_DISK = "compute.disk"
RESOURCE_TYPE_INSTANCE = "compute.instance"
RESOURCE_TYPE_INSTANCE_GROUP = "compute.instance_group"

ZONE = "us-central1-a"
INSTANCE_NAME = generate_unique_name("idem-test-instance")
DISK_NAME = generate_unique_name("idem-test-disk")
NETWORK_NAME = generate_unique_name("idem-test-network")
INSTANCE_GROUP_NAME = generate_unique_name("idem-test-instance-group")
PORT_1_NAME = "http"
PORT_1_PORT = 8080
PORT_2_NAME = "https"
PORT_2_PORT = 8443

PRESENT_STATE_DISK = {
    "name": DISK_NAME,
    "zone": ZONE,
    "project": "tango-gcp",
    "size_gb": "1",
}
PRESENT_STATE_INSTANCE_GROUP = {
    "name": INSTANCE_GROUP_NAME,
    "zone": ZONE,
}


@pytest.fixture(scope="module")
async def gcp_resources(hub, ctx, idem_cli) -> Dict[str, Any]:
    project = hub.tool.gcp.utils.get_project_from_account(ctx, None)

    # Create disk
    disk_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_DISK, {**PRESENT_STATE_DISK}
    )

    assert disk_present_ret["result"], disk_present_ret["comment"]
    disk_created = disk_present_ret["new_state"]
    assert disk_created
    assert disk_created["name"] == DISK_NAME
    assert disk_created["zone"] == ZONE
    assert disk_created["resource_id"]

    # Create instance
    present_state_instance = {
        "name": INSTANCE_NAME,
        "project": project,
        "zone": ZONE,
        "machine_type": f"projects/{project}/zones/{ZONE}/machineTypes/e2-micro",
        "disks": [
            {
                "type_": "PERSISTENT",
                "boot": "True",
                "source": disk_created["resource_id"],
            }
        ],
        "network_interfaces": [
            {
                "network": f"projects/{project}/global/networks/default",
                "subnetwork": f"projects/{project}/regions/us-central1/subnetworks/default",
            }
        ],
    }
    instance_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, {**present_state_instance}
    )

    assert instance_present_ret["result"], instance_present_ret["comment"]
    instance_created = instance_present_ret["new_state"]
    assert instance_created
    assert instance_created.get("name") == INSTANCE_NAME
    assert instance_created.get("zone") == ZONE
    assert len(instance_created.get("disks")) == 1
    assert instance_created.get("resource_id")

    instance_id = instance_created["resource_id"]

    # Create instance group
    instance_group_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE_GROUP, {**PRESENT_STATE_INSTANCE_GROUP}
    )

    assert instance_group_present_ret["result"], instance_group_present_ret["comment"]
    instance_group_created = instance_group_present_ret["new_state"]
    assert instance_group_created
    assert instance_group_created.get("name") == INSTANCE_GROUP_NAME
    assert instance_group_created.get("zone") == ZONE
    assert instance_group_created.get("resource_id")

    instance_group_id = instance_group_created["resource_id"]

    named_ports = [
        {"name": PORT_1_NAME, "port": PORT_1_PORT},
        {"name": PORT_2_NAME, "port": PORT_2_PORT},
    ]

    yield {
        "instance_resource_id": instance_id,
        "instance_group_resource_id": instance_group_id,
        "named_ports": named_ports,
    }


@pytest.mark.asyncio
async def test_list(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.list(ctx)
    assert ret["result"], ret["comment"]

    instance_group_list_returned = ret["ret"]

    assert isinstance(instance_group_list_returned, List)
    assert len(instance_group_list_returned) > 0
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance_group_list_returned[0]["resource_id"], "compute.instance_group"
    )


@pytest.mark.asyncio
async def test_list_filter(hub, ctx, gcp_resources):
    list_instance_group_ret = await hub.exec.gcp.compute.instance_group.list(ctx)
    list_instance_group_ret = list_instance_group_ret["ret"]
    instance_group = list_instance_group_ret[0]

    ret = await hub.exec.gcp.compute.instance_group.list(
        ctx,
        zone=instance_group["zone"],
        filter_=f"name eq {instance_group['name']}",
    )

    assert ret["result"]
    assert not ret["comment"]
    assert ret["ret"]
    instance_group_returned = ret["ret"][0]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance_group_returned["resource_id"], "compute.instance_group"
    )
    assert instance_group_returned["resource_id"] == instance_group["resource_id"]
    assert instance_group_returned["name"] == instance_group["name"]
    assert instance_group_returned["zone"] == instance_group["zone"]


@pytest.mark.asyncio
async def test_list_invalid_filter(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.list(
        ctx,
        zone=ZONE,
        filter_=f"name eq {'invalid-name'}",
    )

    assert ret["result"]
    assert not ret["ret"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.get(
        ctx, resource_id=gcp_resources["instance_group_resource_id"]
    )

    assert ret["result"]
    assert ret["ret"]
    assert not ret["comment"]

    instance_group_returned = ret["ret"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance_group_returned["resource_id"], "compute.instance_group"
    )
    assert (
        instance_group_returned["resource_id"]
        == gcp_resources["instance_group_resource_id"]
    )
    assert instance_group_returned["name"] == INSTANCE_GROUP_NAME
    assert instance_group_returned["zone"] == ZONE


@pytest.mark.asyncio
async def test_get_by_project_zone_name(hub, ctx, gcp_resources):
    list_instance_group_ret = await hub.exec.gcp.compute.instance_group.list(ctx)
    list_instance_group_ret = list_instance_group_ret["ret"]
    instance_group = list_instance_group_ret[0]

    ret = await hub.exec.gcp.compute.instance_group.get(
        ctx,
        zone=instance_group["zone"],
        name=f"{instance_group['name']}",
    )

    assert ret["result"]
    assert ret["ret"]
    assert not ret["comment"]

    instance_group_returned = ret["ret"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance_group_returned["resource_id"], "compute.instance_group"
    )
    assert instance_group_returned["resource_id"] == instance_group["resource_id"]
    assert instance_group_returned["name"] == instance_group["name"]
    assert instance_group_returned["zone"] == instance_group["zone"]


@pytest.mark.asyncio
async def test_get_by_invalid_resource_id(hub, ctx):
    project = hub.tool.gcp.utils.get_project_from_account(ctx, None)
    invalid_resource_id = (
        f"projects/{project}/zones/invalid-zone/instanceGroups/{INSTANCE_GROUP_NAME}"
    )

    ret = await hub.exec.gcp.compute.instance_group.get(
        ctx, resource_id=invalid_resource_id
    )

    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_get_by_invalid_name(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.get(
        ctx,
        zone=ZONE,
        name="invalid-name",
    )

    assert ret["result"]
    assert not ret["ret"]
    assert ret["comment"]


@pytest.mark.dependency(name="add_instances")
@pytest.mark.asyncio
async def test_add_instances(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.add_instances(
        ctx,
        instance_group_resource_id=gcp_resources["instance_group_resource_id"],
        instances=[gcp_resources["instance_resource_id"]],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["name"] == INSTANCE_GROUP_NAME


@pytest.mark.dependency(name="set_named_ports")
@pytest.mark.asyncio
async def test_set_named_ports(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.set_named_ports(
        ctx,
        instance_group_resource_id=gcp_resources["instance_group_resource_id"],
        named_ports=[gcp_resources["named_ports"]],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"].get("named_ports")
    assert len(ret["ret"]["named_ports"]) == 2
    assert {"name": PORT_1_NAME, "port": PORT_1_PORT} in ret["ret"]["named_ports"]
    assert {"name": PORT_2_NAME, "port": PORT_2_PORT} in ret["ret"]["named_ports"]


@pytest.mark.dependency(name="list_instances", depends=["add_instances"])
@pytest.mark.asyncio
async def test_list_instances(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.list_instances(
        ctx,
        instance_group_resource_id=gcp_resources["instance_group_resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) == 1
    assert ret["ret"][0]["instance"].endswith(INSTANCE_NAME)


@pytest.mark.dependency(name="remove_instances", depends=["list_instances"])
@pytest.mark.asyncio
async def test_remove_instances(hub, ctx, gcp_resources):
    ret = await hub.exec.gcp.compute.instance_group.remove_instances(
        ctx,
        instance_group_resource_id=gcp_resources["instance_group_resource_id"],
        instances=[gcp_resources["instance_resource_id"]],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["name"] == INSTANCE_GROUP_NAME


@pytest.mark.asyncio
@pytest.fixture(autouse=True, scope="module")
async def cleanup_resources(hub, ctx, gcp_project):
    try:
        yield
    finally:
        delete_ret = await hub.exec.gcp_api.client.compute.instance_group.delete(
            ctx,
            project=gcp_project,
            zone=ZONE,
            instance_group=INSTANCE_GROUP_NAME,
        )
        assert (
            delete_ret["result"] or "notFound" in delete_ret["comment"][0]
        ), delete_ret["comment"]
        if (
            delete_ret["result"]
            and delete_ret.get("ret", {}).get("kind") == "compute#operation"
        ):
            handle_operation_ret = await hub.tool.gcp.operation_utils.handle_operation(
                ctx,
                {"operation_id": delete_ret["ret"]["selfLink"]},
                "compute.instance_group",
                wait_until_done=True,
            )
            assert handle_operation_ret["result"], handle_operation_ret["comment"]

        delete_ret = await hub.exec.gcp_api.client.compute.instance.delete(
            ctx, project=gcp_project, zone=ZONE, instance=INSTANCE_NAME
        )
        assert (
            delete_ret["result"] or "notFound" in delete_ret["comment"][0]
        ), delete_ret["comment"]
        if (
            delete_ret["result"]
            and delete_ret.get("ret", {}).get("kind") == "compute#operation"
        ):
            handle_operation_ret = await hub.tool.gcp.operation_utils.handle_operation(
                ctx,
                {"operation_id": delete_ret["ret"]["selfLink"]},
                "compute.instance",
                wait_until_done=True,
            )
            assert handle_operation_ret["result"], handle_operation_ret["comment"]

        delete_ret = await hub.exec.gcp_api.client.compute.disk.delete(
            ctx, project=gcp_project, zone=ZONE, disk=DISK_NAME
        )
        assert (
            delete_ret["result"] or "notFound" in delete_ret["comment"][0]
        ), delete_ret["comment"]
