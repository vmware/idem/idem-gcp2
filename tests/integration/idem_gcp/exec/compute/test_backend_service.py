import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "name": generate_unique_name("idem-test-backends"),
    "region": "us-central1",
    "health_check_name": generate_unique_name("idem-test-backends"),
}

RESOURCE_TYPE_BACKEND_SERVICE = "compute.backend_service"
RESOURCE_TYPE_HEALTH_CHECK = "compute.health_check"

HEALTH_CHECK_SPEC = """
{health_check_name}:
  gcp.compute.health_check.present:
  - name: {health_check_name}
  - type_: TCP
  - tcp_health_check:
      port: 80
"""

BACKEND_SERVICE_SPEC = """
{name}:
    gcp.compute.backend_service.present:
      - name: {name}
      - description: Idem Backend service exec test
      - health_checks:
        - projects/tango-gcp/global/healthChecks/{health_check_name}
      - timeout_sec: 60
      - port: 80
      - protocol: HTTP
      - port_name: http
      - session_affinity: NONE
      - affinity_cookie_ttl_sec: 10
      - region: {region}
      - load_balancing_scheme: INTERNAL_MANAGED
      - locality_lb_policy: ROUND_ROBIN
      - connection_draining:
          draining_timeout_sec: 300
"""


@pytest.fixture(scope="module")
def backend_service(hub, idem_cli):
    # Create backend service
    hc_present_state_sls = HEALTH_CHECK_SPEC.format(**PARAMETER)

    hc_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        hc_present_state_sls,
    )

    assert hc_ret["result"], hc_ret["comment"]
    resource_id = hc_ret["new_state"]["resource_id"]
    PARAMETER["health_check_resource_id"] = resource_id

    bs_present_state_sls = BACKEND_SERVICE_SPEC.format(**PARAMETER)
    bs_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        bs_present_state_sls,
    )

    assert bs_ret["result"], bs_ret["comment"]
    resource_id = bs_ret["new_state"]["resource_id"]
    PARAMETER["backend_service_resource_id"] = resource_id

    try:
        yield bs_ret["new_state"]
    finally:
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_BACKEND_SERVICE,
            PARAMETER["name"],
            PARAMETER["backend_service_resource_id"],
        )
        if not ret["result"]:
            hub.log.warning(
                f"Could not cleanup backend service {PARAMETER['backend_service_resource_id']}",
                ret["comment"],
            )

        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_HEALTH_CHECK,
            PARAMETER["health_check_name"],
            PARAMETER["health_check_resource_id"],
        )
        if not ret["result"]:
            hub.log.warning(
                f"Could not cleanup health check {PARAMETER['health_check_resource_id']}",
                ret["comment"],
            )


@pytest.mark.asyncio
async def test_get_missing_name_arg(hub, ctx):
    ret = await hub.exec.gcp.compute.backend_service.get(ctx)
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]
    assert 'Missing required parameter "backendService"' in ret["comment"]


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, backend_service):
    ret = await hub.exec.gcp.compute.backend_service.get(
        ctx, name=backend_service["name"], region=PARAMETER["region"]
    )
    assert ret["result"], ret["comment"]
    assert not ret["comment"]
    assert backend_service["resource_id"] == ret["ret"].get("resource_id", None)
    assert backend_service["name"] in ret["ret"].get("name", None)


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, backend_service):
    ret = await hub.exec.gcp.compute.backend_service.get(
        ctx, resource_id=backend_service["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"], ret["comment"]
    assert not ret["comment"]
    assert backend_service["resource_id"] == ret["ret"].get("resource_id", None)
    assert backend_service["name"] == ret["ret"].get("name", None)


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx):
    invalid_name = "non-existing-backend-service"
    ret = await hub.exec.gcp.compute.backend_service.get(
        ctx,
        name=invalid_name,
        region=PARAMETER["region"],
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("result is empty" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_aggregated_list(hub, ctx, backend_service):
    ret = await hub.exec.gcp.compute.backend_service.list(ctx)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) > 0


@pytest.mark.asyncio
async def test_list(hub, ctx, backend_service):
    ret = await hub.exec.gcp.compute.backend_service.list(
        ctx, region=PARAMETER["region"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) > 0


@pytest.mark.asyncio
async def test_list_filter_wrong_region(hub, ctx, backend_service):
    ret = await hub.exec.gcp.compute.backend_service.list(
        ctx, region="europe-west1", filter_=f"name={backend_service['name']}"
    )
    assert ret["result"], ret["comment"]
    assert not ret["comment"]
    assert len(ret["ret"]) == 0
