import re

import pytest

from tests.utils import generate_unique_name

# How to create a service account key: https://cloud.google.com/iam/docs/keys-upload
PUBLIC_KEY_DATA = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUNvRENDQVlnQ0NRRCtRT0dwNWR1Y2VUQU5CZ2txaGtpRzl3MEJBUXNGQURBUk1ROHdEUVlEVlFRRERBWjEKYm5WelpXUXdJQmNOTWpNd05EQTNNVFEwTkRJMVdoZ1BNakExTURBNE1qTXhORFEwTWpWYU1CRXhEekFOQmdOVgpCQU1NQm5WdWRYTmxaRENDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFLTGhrNVBaCi9hbGJQbG8rU01zbnNIcjNzSzRrOGl5aEJpblZ1QU1ySERRQktCaFlnZlVBV1JkOHdXT0VsRnNucXV1UVpSTzEKekVwNmpOZHZUaXpycWFuSmdKcU9XNE80YnhBRytBSlY4cjFENmxVM2hsZmc4bExHRkhjeXQwc1NlaDQwSi9DYQpOUHFKZUlIeGtyb2o0TGFBZEhuMGFTRkQ3RU5lSWFjcUFXTVYrOHNwamZtR3dRVFRIR2pJSDZPOFJDZVRiU0owCjl6L2xjZ0RuL2hxcURwSHd6SmE5U0lLakpUTDM5cVBYS2lwdlJadTFucW43QVRQNjI2OXllTEo2Tm51emFGc3EKcXNhOEpiSmdsZmNtV2lXZmVwdHBNR3ZTTEprMENjVFZlQ0IwcHhxb3FMd3ZqTDA1djVUeHJSRnhCcXBBUGphNwo0Y09qVXhzTDB4bWQvakVDQXdFQUFUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFuS0dSL2pTYmcvUnNENEFCCjUybE8yQ2ZxRnB0aWdDdW5lQ05kTCtjM01hcXlHWDdiRlFMZlVvc1RJWk5qZFduRkc2U21RclRScjBaei9SMTMKcTlFRVRhZjAwOWpoM25aVzZLeU93MmJiMUx1Y0dDbHhaWHV0eVlIYkg4NWxDODk5L2FtbEpsdGJyc3RwUC9zTwpDb0VrQmpDbkRPRzNXZkNqRm5ITEJtODgvY1E1ZVZ2aERtRHZqcFVjVGlVbUZRNG5Ccmx0ZUNYWlRSNkdROFgvCmw3MlhCekJBTHc3QkV1MUFuL0srMldJeGVtbjEwV2plUnFadVJhZFRsNThkUE9ac2JtTGlOanVZQzBsdDFTZDEKaWVpV1ljTW8xMkVrenZnd0dFR01XMHFwbTluVjZBWU1RU3R1NFd1SDh1S0NYejNmZWk0N09EWGt1aGZvL1ZYTwprb0tCMlE9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t"
NAME = generate_unique_name("idem-account", 30)
SERVICE_ACCOUNT_STATE = {
    "name": NAME,
    "account_id": NAME,
    "description": "Created by automated gcp-idem IT for service account keys.",
    "display_name": "idem-gcp-it",
}
NAME = generate_unique_name("idem-account", 30)
SERVICE_ACCOUNT_KEY_STATE = {
    "private_key_type": "TYPE_PKCS12_FILE",
    "key_algorithm": "KEY_ALG_RSA_2048",
    "name": "idem-gcp-it",
}
EXEC_SA_SLS = """
random-name:
  exec.run:
  - path: gcp.iam.service_account_key.{api_method}
  - kwargs:
      resource_id: {resource_id}
"""


@pytest.fixture(scope="module")
async def service_account(hub, ctx, idem_cli, cleaner):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "iam.service_account", SERVICE_ACCOUNT_STATE
    )
    assert ret["result"], ret["comment"]
    yield ret["new_state"]
    cleaner(ret["new_state"], "iam.service_account")


@pytest.fixture(scope="module")
async def service_account_key(hub, ctx, idem_cli, service_account, cleaner):
    key_sls = {
        **SERVICE_ACCOUNT_KEY_STATE,
        "service_account_id": service_account["resource_id"],
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "iam.service_account_key", key_sls
    )
    assert ret["result"], ret["comment"]
    yield ret["new_state"]
    cleaner(ret["new_state"], "iam.service_account_key")


@pytest.mark.asyncio
async def test_get_name(hub, ctx, service_account_key):
    ret = await hub.exec.gcp.iam.service_account_key.get(
        ctx,
        name=service_account_key["resource_id"],
    )

    assert ret["result"], ret["ret"]
    assert not ret["comment"]

    returned_service_account_key = ret["ret"]
    assert service_account_key["name"] == returned_service_account_key["name"]
    assert (
        service_account_key["key_origin"] == returned_service_account_key["key_origin"]
    )
    assert service_account_key["key_type"] == returned_service_account_key["key_type"]
    assert service_account_key.get("resource_id") == returned_service_account_key.get(
        "resource_id"
    )


@pytest.mark.asyncio
async def test_upload(hub, ctx, service_account):
    ret = await hub.exec.gcp.iam.service_account_key.upload(
        ctx,
        service_account_id=service_account["resource_id"],
        public_key_data=PUBLIC_KEY_DATA,
    )
    assert ret["result"], ret["comment"]
    service_account_key = ret["ret"]
    assert service_account_key["key_algorithm"] == "KEY_ALG_RSA_2048"
    assert service_account_key["key_origin"] == "USER_PROVIDED"
    assert service_account_key["key_type"] == "USER_MANAGED"
    assert re.match(
        r"^" + service_account["resource_id"] + r"/keys/.+$",
        service_account_key["resource_id"],
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="test_upload")
async def test_disable_enable(hub, ctx, idem_cli, service_account_key):
    state = EXEC_SA_SLS.format(
        resource_id=service_account_key["resource_id"], api_method="disable"
    )
    disable_ret = hub.tool.utils.call_exec_from_sls(idem_cli, state)
    assert disable_ret["result"], disable_ret["comment"]

    ret = await hub.exec.gcp.iam.service_account_key.get(
        ctx, resource_id=service_account_key["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["disabled"]

    state = EXEC_SA_SLS.format(
        resource_id=service_account_key["resource_id"], api_method="enable"
    )
    enable_ret = hub.tool.utils.call_exec_from_sls(idem_cli, state)
    assert enable_ret["result"], enable_ret["comment"]

    ret = await hub.exec.gcp.iam.service_account_key.get(
        ctx, resource_id=service_account_key["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert "disabled" not in ret["ret"]
