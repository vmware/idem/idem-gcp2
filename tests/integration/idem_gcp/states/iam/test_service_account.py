import random
from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
NAME = generate_unique_name("idem-account", 30)
PARAMETER = {
    "name": NAME,
    "account_id": NAME,
    "description": "Created by automated gcp-idem IT for service accounts.",
    "display_name": "idem-gcp-it",
}

RESOURCE_TYPE = "iam.service_account"
GCP_RESOURCE_TYPE = f"gcp.{RESOURCE_TYPE}"

PRESENT_CREATE_STATE = {**PARAMETER}


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.iam.service_account.describe(ctx)
    for resource_id in ret:
        assert "gcp.iam.service_account.present" in ret[resource_id]
        described_resource = ret[resource_id].get("gcp.iam.service_account.present")
        assert described_resource
        service_account = dict(ChainMap(*described_resource))
        assert service_account.get("resource_id") == resource_id


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_present_create(hub, idem_cli, tests_dir, __test, cleaner):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], RESOURCE_TYPE)

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=GCP_RESOURCE_TYPE,
                name=ret["new_state"]["resource_id"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=GCP_RESOURCE_TYPE,
                name=ret["new_state"]["resource_id"],
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_update")
def test_service_account_present_update(hub, idem_cli, tests_dir, __test, cleaner):
    global PARAMETER
    PRESENT_CREATE_STATE["account_id"] = "account-" + str(random.randint(1111, 9999))
    PRESENT_CREATE_STATE["name"] = PRESENT_CREATE_STATE["account_id"]
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], RESOURCE_TYPE)

    update_state = {
        **ret["new_state"],
        "description": "update operation applied",
        "display_name": "idem-gcp-it-update",
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_state, __test
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=GCP_RESOURCE_TYPE, name=ret["new_state"]["resource_id"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=GCP_RESOURCE_TYPE, name=ret["new_state"]["resource_id"]
            )
            in ret["comment"]
        )

    assert update_state["description"] == ret["new_state"]["description"]
    assert update_state["display_name"] == ret["new_state"]["display_name"]


@pytest.mark.asyncio
def test_update_with_random_name(hub, idem_cli, cleaner):
    global PARAMETER
    PRESENT_CREATE_STATE["account_id"] = "account-" + str(random.randint(1111, 9999))
    PRESENT_CREATE_STATE["name"] = PRESENT_CREATE_STATE["account_id"]
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], RESOURCE_TYPE)

    # name is different from resource_id
    update_state = {
        "resource_id": ret["new_state"]["resource_id"],
        "name": "random-str",
        "description": "update operation applied",
        "display_name": "idem-gcp-it-update",
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE,
        update_state,
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    assert (
        hub.tool.gcp.comment_utils.update_comment(
            resource_type=GCP_RESOURCE_TYPE, name=ret["new_state"]["resource_id"]
        )
        in ret["comment"]
    )

    assert update_state["description"] == ret["new_state"]["description"]
    assert update_state["display_name"] == ret["new_state"]["display_name"]


@pytest.mark.asyncio
def test_update_with_empty_string(hub, idem_cli, cleaner):
    global PARAMETER
    PRESENT_CREATE_STATE["account_id"] = "account-" + str(random.randint(1111, 9999))
    PRESENT_CREATE_STATE["name"] = PRESENT_CREATE_STATE["account_id"]
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], RESOURCE_TYPE)

    update_state = {
        "resource_id": ret["new_state"]["resource_id"],
        "name": ret["new_state"]["name"],
        "description": "",
        "display_name": "",
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE,
        update_state,
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]
    assert ret["new_state"].get("description") == ""
    assert ret["new_state"].get("display_name") == ""


@pytest.mark.dependency(name="present_update_read_only_field")
def test_service_account_present_update_read_only_field(hub, idem_cli, cleaner):
    global PARAMETER
    PRESENT_CREATE_STATE["account_id"] = "account-" + str(random.randint(1111, 9999))
    PRESENT_CREATE_STATE["name"] = PRESENT_CREATE_STATE["account_id"]
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], RESOURCE_TYPE)

    updated_email = "updated@test.com"
    update_state = {**ret["new_state"], "email": updated_email}
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_state
    )

    assert not ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            GCP_RESOURCE_TYPE,
            ret["new_state"]["name"],
            ("email",),
        )
        in ret["comment"]
    )
