from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

PARAMETER = {
    "network_name": generate_unique_name("idem-test-network"),
    "network2_name": generate_unique_name("idem-test-network"),
    "peering_name1": generate_unique_name("idem-test-peering"),
    "peering_name2": generate_unique_name("idem-test-peering"),
    "routing_mode": "REGIONAL",
    "auto_create_subnetworks_default": True,
}

NETWORK_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: {auto_create_subnetworks}
  - peerings: []
  - routing_config:
      routing_mode: {routing_mode}
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

NETWORK_WITH_PEERING_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: {auto_create_subnetworks}
  - routing_config:
      routing_mode: REGIONAL
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
  - peerings:
    - auto_create_routes: true
      exchange_subnet_routes: true
      export_custom_routes: false
      export_subnet_routes_with_public_ip: true
      import_custom_routes: false
      import_subnet_routes_with_public_ip: false
      name: {peering_name}
      network: {peering_network}
      peer_mtu: 1520
"""

RESOURCE_TYPE_NETWORK = "compute.network"
FULL_RESOURCE_TYPE_NETWORK = f"gcp.{RESOURCE_TYPE_NETWORK}"
# Not all non updatable properties are listed
NON_UPDATABLE_PROPERTIES_PATHS = {"auto_create_subnetworks"}


@pytest.fixture(scope="module")
def test_networks(hub, idem_cli, cleaner):
    global PARAMETER
    # Create network
    present_state_sls = NETWORK_SPEC.format(
        **{
            "network_name": PARAMETER["network_name"],
            "routing_mode": PARAMETER["routing_mode"],
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, present_state_sls)

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.create_comment(
            resource_type=FULL_RESOURCE_TYPE_NETWORK, name=PARAMETER["network_name"]
        )
        in ret["comment"]
    )

    network = ret["new_state"]
    assert network["id_"]
    assert network, "Network creation successful but new state is None"
    PARAMETER["test_network"] = network
    cleaner(network, "compute.network")

    present_state_sls = NETWORK_WITH_PEERING_SPEC.format(
        **{
            "network_name": PARAMETER["network2_name"],
            "peering_name": PARAMETER["peering_name1"],
            "peering_network": network.get("resource_id"),
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )

    ret = hub.tool.utils.call_present_from_sls(idem_cli, present_state_sls)

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.create_comment(
            resource_type=FULL_RESOURCE_TYPE_NETWORK,
            name=PARAMETER["network2_name"],
        )
        in ret["comment"]
    )

    network = ret["new_state"]
    assert network, "Network creation successful but new state is None"
    PARAMETER["test_network2"] = network
    cleaner(network, "compute.network")

    networks = [PARAMETER["test_network"], PARAMETER["test_network2"]]
    yield networks


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_patch_network")
def test_present_patch_network(ctx, hub, idem_cli, test_networks, __test):
    global PARAMETER
    current_network = test_networks[0]

    assert current_network.get("routing_config")
    assert current_network["routing_config"].get("routing_mode") == "REGIONAL"
    PARAMETER["routing_mode"] = "GLOBAL"

    network_update_sls = NETWORK_SPEC.format(
        **{
            "network_name": current_network["name"],
            "routing_mode": PARAMETER["routing_mode"],
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, network_update_sls, __test)

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=FULL_RESOURCE_TYPE_NETWORK, name=PARAMETER["network_name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=FULL_RESOURCE_TYPE_NETWORK, name=PARAMETER["network_name"]
            )
            in ret["comment"]
        )

        updated_network = ret["new_state"]
        assert updated_network.get("routing_config")
        assert current_network.get("routing_config")
        assert updated_network["routing_config"].get("routing_mode") != current_network[
            "routing_config"
        ].get("routing_mode")


@pytest.mark.dependency(
    name="present_remove_add_peering", depends=["present_patch_network"]
)
def test_present_remove_add_peering(hub, idem_cli, test_networks):
    assert len(test_networks) == 2

    assert not test_networks[0].get("peerings")
    assert len(test_networks[1].get("peerings")) == 1

    network_with_peerings = test_networks[1]
    network_to_update_name = network_with_peerings["name"]

    # Remove peerings
    network_update_sls = NETWORK_SPEC.format(
        **{
            "network_name": network_to_update_name,
            "routing_mode": PARAMETER["routing_mode"],
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, network_update_sls)
    assert ret["result"], ret["comment"]
    updated_network_no_peerings = ret["new_state"]

    assert updated_network_no_peerings.get("name") == network_to_update_name
    assert len(updated_network_no_peerings.get("peerings", [])) == 0

    # Add peerings
    network_to_peer_with = test_networks[0]
    network_update_sls_with_peerings = NETWORK_WITH_PEERING_SPEC.format(
        **{
            "network_name": network_to_update_name,
            "peering_name": PARAMETER["peering_name1"],
            "peering_network": network_to_peer_with.get("resource_id"),
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(
        idem_cli, network_update_sls_with_peerings
    )
    assert ret["result"], ret["comment"]
    updated_network_with_peerings = ret["new_state"]

    assert updated_network_with_peerings.get("name") == network_to_update_name
    assert len(updated_network_with_peerings.get("peerings", [])) == 1


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="present_peering_activation_same_network_ip_range_should_fail",
    depends=["present_remove_add_peering"],
)
async def test_present_peering_activation_same_network_ip_range_should_fail(
    hub, ctx, idem_cli, gcp_project, test_networks
):
    assert len(test_networks) == 2

    assert not test_networks[0].get("peerings")
    assert len(test_networks[1].get("peerings")) == 1

    network_wo_peerings = test_networks[0]
    network_to_update_name = network_wo_peerings["name"]

    # Add peering attempt when the peering network also has a peering to this one
    network_to_peer_with = test_networks[1]
    network_update_sls_with_peerings = NETWORK_WITH_PEERING_SPEC.format(
        **{
            "network_name": network_to_update_name,
            "peering_name": PARAMETER["peering_name2"],
            "peering_network": network_to_peer_with.get("resource_id"),
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(
        idem_cli, network_update_sls_with_peerings
    )

    assert not ret["result"], ret["comment"]
    error_comments = [
        comment for comment in ret.get("comment", []) if "errors" in comment
    ]
    assert error_comments
    unsupported_operation_comment = next(
        error for error in error_comments if "UNSUPPORTED_OPERATION" in error
    )
    assert unsupported_operation_comment

    # Get the network
    get_ret = await hub.exec.gcp.compute.network.get(
        ctx, project=gcp_project, name=network_to_update_name
    )
    network = get_ret["ret"]
    assert network_to_update_name == network["name"]

    # Confirm that peering is not added
    assert len(network.get("peerings", [])) == 0


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_changed_non_updatable_properties",
    depends=["present_remove_add_peering"],
)
def test_network_present_changed_non_updatable_properties(
    hub, idem_cli, tests_dir, __test, test_networks
):
    network = test_networks[0]

    # Make a change to auto_create_subnetworks, which is a non-updatable property
    network_update_sls = NETWORK_SPEC.format(
        **{
            "network_name": network["name"],
            "routing_mode": PARAMETER["routing_mode"],
            "auto_create_subnetworks": False,
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, network_update_sls, __test)

    assert not ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            FULL_RESOURCE_TYPE_NETWORK,
            network["name"],
            NON_UPDATABLE_PROPERTIES_PATHS,
        )
    ] == ret["comment"]

    assert ret["new_state"] == ret["old_state"]

    # Asserting that in the new state, auto_create_subnetworks did not change to False as requested
    assert ret["new_state"].get("auto_create_subnetworks")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_network_wo_peerings_empty_update",
    depends=["present_patch_network"],
)
def test_network_wo_peerings_empty_update(
    hub, idem_cli, tests_dir, __test, test_networks
):
    network = test_networks[0]

    # Create the sls, with no changes to it
    network_update_sls = NETWORK_SPEC.format(
        **{
            "network_name": network["name"],
            "routing_mode": PARAMETER["routing_mode"],
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, network_update_sls, __test)

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            FULL_RESOURCE_TYPE_NETWORK,
            network["name"],
        )
    ] == ret["comment"]

    assert ret["new_state"] == ret["old_state"]
    assert ret["new_state"]["name"] == network["name"]
    assert (
        ret["new_state"]["routing_config"]["routing_mode"] == PARAMETER["routing_mode"]
    )
    assert (
        ret["new_state"]["auto_create_subnetworks"]
        == PARAMETER["auto_create_subnetworks_default"]
    )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_network_with_peerings_empty_update",
    depends=["present_remove_add_peering"],
)
def test_network_with_peerings_empty_update(
    hub, idem_cli, tests_dir, __test, test_networks
):
    network = test_networks[1]
    peer_network = test_networks[0]

    # Create the sls, with no changes to it
    network_update_sls = NETWORK_WITH_PEERING_SPEC.format(
        **{
            "network_name": PARAMETER["network2_name"],
            "peering_name": PARAMETER["peering_name1"],
            "peering_network": peer_network.get("resource_id"),
            "auto_create_subnetworks": PARAMETER["auto_create_subnetworks_default"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, network_update_sls, __test)

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            FULL_RESOURCE_TYPE_NETWORK,
            network["name"],
        )
    ] == ret["comment"]

    assert ret["new_state"] == ret["old_state"]
    assert ret["new_state"]["name"] == network["name"]
    new_state_peering = ret["new_state"]["peerings"][0]
    assert new_state_peering["name"] == PARAMETER["peering_name1"]
    assert new_state_peering["network"] == peer_network.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="describe", depends=["present_network_with_peerings_empty_update"]
)
async def test_network_describe(hub, ctx):
    ret = await hub.states.gcp.compute.network.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.network.present")
        assert described_resource
        network = dict(ChainMap(*described_resource))
        assert network.get("resource_id") == resource_id


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
def test_network_absent(hub, idem_cli, tests_dir, __test, test_networks):
    global PARAMETER
    network_to_delete = test_networks[0]
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_NETWORK,
        network_to_delete["name"],
        network_to_delete["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=FULL_RESOURCE_TYPE_NETWORK,
                name=network_to_delete["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=FULL_RESOURCE_TYPE_NETWORK,
                name=network_to_delete["name"],
            )
            in ret["comment"]
        )

    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
