from typing import List

import pytest

from idem_gcp.tool.gcp.resolver import CompositeContractMod


@pytest.fixture(scope="module")
def composite_mod(hub) -> CompositeContractMod:
    yield hub.tool.gcp.resolver.resolve_sub("gcp.compute.disk.get", hub.tool.gcp.API)


def test_find_best_matching_mod(hub, composite_mod: CompositeContractMod):
    best_matching_mod, err = composite_mod.find_best_matching_mod(**{"zone": "zone-1"})
    assert best_matching_mod
    assert getattr(best_matching_mod, "_native_resource_type") == "compute.disks"
    assert not err

    best_matching_mod, err = composite_mod.find_best_matching_mod(
        **{"region": "region-1"}
    )
    assert best_matching_mod
    assert getattr(best_matching_mod, "_native_resource_type") == "compute.regionDisks"
    assert not err

    mod, err = composite_mod.find_best_matching_mod(**{"zoneX": "zone-1"})
    assert not mod
    assert err

    mod, err = composite_mod.find_best_matching_mod(**{"regionX": "region-1"})
    assert not mod
    assert err

    mod, err = composite_mod.find_best_matching_mod(
        **{"zone": "zone-1", "region": "region-1"}
    )
    assert not mod
    assert err

    mod, err = composite_mod.find_best_matching_mod(
        **{"zoneX": "zone-1", "regionX": "region-1"}
    )
    assert not mod
    assert err


def test_compute_matching_score(hub, composite_mod: CompositeContractMod):
    resource_paths: List[str] = hub.tool.gcp.resource_prop_utils.get_resource_paths(
        "compute.disk"
    )
    assert resource_paths
    assert len(resource_paths) == 2

    zonal_disk_path: str = resource_paths[0]
    regional_disk_path: str = resource_paths[1]

    score = composite_mod.compute_matching_score(
        zonal_disk_path, **{"region": "region-1"}
    )
    assert score == 0
    score = composite_mod.compute_matching_score(zonal_disk_path, **{"zone": "zone-1"})
    assert score == 1
    score = composite_mod.compute_matching_score(
        zonal_disk_path, **{"zone": "zone-1", "project": "project-1"}
    )
    assert score == 2
    score = composite_mod.compute_matching_score(
        zonal_disk_path, **{"regionX": "region-1"}
    )
    assert score == 0
    score = composite_mod.compute_matching_score(
        zonal_disk_path, **{"zone": "zone-1", "region": "region-1"}
    )
    assert score == 1

    score = composite_mod.compute_matching_score(
        regional_disk_path, **{"region": "region-1"}
    )
    assert score == 1
    score = composite_mod.compute_matching_score(
        regional_disk_path, **{"zone": "zone-1"}
    )
    assert score == 0
    score = composite_mod.compute_matching_score(
        regional_disk_path, **{"region": "region-1", "project": "project-1"}
    )
    assert score == 2
    score = composite_mod.compute_matching_score(
        regional_disk_path, **{"regionX": "region-1"}
    )
    assert score == 0
    score = composite_mod.compute_matching_score(
        regional_disk_path, **{"zone": "zone-1", "region": "region-1"}
    )
    assert score == 1
