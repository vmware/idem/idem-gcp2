Executing long-running operations
=================================

Some GCP methods are long-running and asynchronous. They return status objects, which need to be subsequently queried and waited for.

The GCP plugin handles these operations on you behalf by waiting until the method completes before returning the final result.

That means asynchronous GCP operations become synchronous Idem GCP exec calls.

An example for this is the `stop` method of the `instance` resource.

Invoking this method directly through the `GCP API <https://cloud.google.com/compute/docs/reference/rest/v1/instances/stop>`_
would result in an Operation object returned immediately.

However, stopping an instance with idem:

``idem exec gcp.compute.instance.stop project=<project_name> zone=<zone_name> instance=<instance_name>``

is a slow operation which returns only once the VM has stopped and the result is the current instance state,
where the `status: TERMINATED` can be observed.
