from idem_gcp.tool.gcp.generate.exec_context import ExecutionContext


async def sig_should_operate(hub, execution_context: ExecutionContext) -> bool:
    pass


async def sig_operate(hub, execution_context: ExecutionContext) -> None:
    pass
