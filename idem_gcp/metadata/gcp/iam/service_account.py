"""Metadata module for managing Service Account."""

PATH = [
    "projects/{project_id}/serviceAccounts/{email}",
    "projects/{project_id}/serviceAccounts/{unique_id}",
]

NATIVE_RESOURCE_TYPE = "iam.projects.service_accounts"
