"""Metadata module for managing Region Operations."""

PATH = "projects/{project}/regions/{region}/operations/{operation}"

NATIVE_RESOURCE_TYPE = "compute.region_operations"
