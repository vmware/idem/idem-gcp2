"""Metadata module for managing Machine Types."""

PATH = "projects/{project}/zones/{zone}/machineTypes/{machineType}"

NATIVE_RESOURCE_TYPE = "compute.machine_types"
