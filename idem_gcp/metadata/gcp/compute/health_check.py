"""Metadata module for managing HealthChecks."""

PATH = "projects/{project}/global/healthChecks/{healthCheck}"

NATIVE_RESOURCE_TYPE = "compute.health_checks"
