"""Metadata module for managing Backend services."""
# For now, we support only regional backend services.
PATH = "projects/{project}/regions/{region}/backendServices/{backendService}"

NATIVE_RESOURCE_TYPE = "compute.backend_services"
