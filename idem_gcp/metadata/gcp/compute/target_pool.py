"""Metadata module for managing Target Pools."""

PATH = [
    "projects/{project}/regions/{region}/targetPools/{targetPool}",
]

NATIVE_RESOURCE_TYPE = [
    "compute.targetPools",
]
