"""Metadata module for managing region scoped Backend services."""

PATH = "projects/{project}/regions/{region}/backendServices/{backendService}"

NATIVE_RESOURCE_TYPE = "compute.region_backend_services"
