google-api-python-client>=2.88.0
idem>=21.1.0,!=23.1.3
aiohttp>=3.8.4
idem-aiohttp>=3.0.0
pop>=26.0.3
deepdiff>=5.8.1
importlib_resources>=5.9.0
PyYaml>=6.0.1
